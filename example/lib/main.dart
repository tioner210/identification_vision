import 'package:flutter/material.dart';
import 'package:identification_vision/identification_vision.dart';

void main(List<String> args) {
  runApp(MaterialApp(
    home: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Identification? _id;
  Identification? _id50;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          _id?.image != null ? Image.memory(_id!.image!) : const SizedBox(),
          _id?.cve != null ? Text(_id!.cve) : const SizedBox(),
          ElevatedButton(
              onPressed: () async {
                final id = await launchIdentificationVision(context);
                setState(() {
                  _id = id;
                });
              },
              child: Center(child: const Text("Capturar ine"))),
          _id50?.image != null ? Image.memory(_id50!.image!) : const SizedBox(),
          _id50?.cve != null ? Text(_id50!.cve) : const SizedBox(),
          ElevatedButton(
              onPressed: () async {
                final id50 = await launchIdentificationVision(context,
                    quality: 80, height: 510, width: 700);
                setState(() {
                  _id50 = id50;
                });
              },
              child: Center(child: const Text("Capturar ine"))),
        ],
      ),
    );
  }
}
