import 'dart:typed_data';

import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:google_ml_kit/google_ml_kit.dart';
import 'package:image/image.dart' as im;

class ImageUtils {
  static ImageUtils? _instance;

  ImageUtils._();

  factory ImageUtils.instance() {
    _instance ??= ImageUtils._();
    return _instance!;
  }

  Uint8List getUint8ListFromcameraImage(CameraImage cameraImage) {
    final WriteBuffer allBytes = WriteBuffer();
    for (Plane plane in cameraImage.planes) {
      allBytes.putUint8List(plane.bytes);
    }
    final bytes = allBytes.done().buffer.asUint8List();
    return bytes;
  }

  im.Image _convertYUV420(CameraImage image) {
    var img = im.Image(image.width, image.height); // Create Image buffer

    const int shift = (0xFF << 24);
    final int width = image.width;
    final int height = image.height;
    final int uvRowStride = image.planes[1].bytesPerRow;
    final int uvPixelStride = image.planes[1].bytesPerPixel ?? 0;

    for (int x = 0; x < width; x++) {
      for (int y = 0; y < height; y++) {
        final int uvIndex =
            uvPixelStride * (x / 2).floor() + uvRowStride * (y / 2).floor();
        final int index = y * width + x;

        final yp = image.planes[0].bytes[index];
        final up = image.planes[1].bytes[uvIndex];
        final vp = image.planes[2].bytes[uvIndex];
        // Calculate pixel color
        int r = (yp + vp * 1436 / 1024 - 179).round().clamp(0, 255);
        int g = (yp - up * 46549 / 131072 + 44 - vp * 93604 / 131072 + 91)
            .round()
            .clamp(0, 255);
        int b = (yp + up * 1814 / 1024 - 227).round().clamp(0, 255);
        // color: 0x FF  FF  FF  FF
        //           A   B   G   R
        img.data[index] = shift | (b << 16) | (g << 8) | r;
      }
    }

    return img;
  }

  InputImage getInputImagefromCameraImage(CameraImage cameraImage,
      [int sensorOrientation = 0]) {
    final bytes = getUint8ListFromcameraImage(cameraImage);

    final Size imageSize =
        Size(cameraImage.width.toDouble(), cameraImage.height.toDouble());

    final InputImageRotation imageRotation =
        InputImageRotationMethods.fromRawValue(sensorOrientation) ??
            InputImageRotation.Rotation_0deg;

    final InputImageFormat inputImageFormat =
        InputImageFormatMethods.fromRawValue(cameraImage.format.raw) ??
            InputImageFormat.NV21;

    final planeData = cameraImage.planes
        .map((Plane plane) => InputImagePlaneMetadata(
              bytesPerRow: plane.bytesPerRow,
              height: plane.height,
              width: plane.width,
            ))
        .toList();

    final inputImageData = InputImageData(
      size: imageSize,
      imageRotation: imageRotation,
      inputImageFormat: inputImageFormat,
      planeData: planeData,
    );

    final inputImage =
        InputImage.fromBytes(bytes: bytes, inputImageData: inputImageData);
    return inputImage;
  }

  Future<im.Image> cameraImageToImageLib(CameraImage image) async {
    final esYUV = image.format.group == ImageFormatGroup.yuv420;
    final esBGRA = image.format.group == ImageFormatGroup.bgra8888;
    late im.Image img;
    if (esYUV) {
      img = _convertYUV420(image);
    } else if (esBGRA) {
      img = _convertBGRA8888(image);
    }
    return img;
  }

  Uint8List convertImageLibtoMemoryUint8List(im.Image image,
      {int quality = 100}) {
    final jpg = im.encodeJpg(image, quality: quality);
    // List<int> png = pngEncoder.encodeImage(
    //   image,
    // );
    return Uint8List.fromList(jpg);
  }

  Future<Uint8List?> convertCameraImageToMemoryUint8List(
      CameraImage cameraImage) async {
    try {
      final imgage = await cameraImageToImageLib(cameraImage);
      return convertImageLibtoMemoryUint8List(imgage);
    } catch (e) {
      if (kDebugMode) {
        print(">>>>>>>>>>>> ERROR:" + e.toString());
      }
      return null;
    }
  }

  im.Image _convertBGRA8888(CameraImage image) {
    return im.Image.fromBytes(
      (image.planes[0].bytesPerRow / 4).round(),
      image.height,
      image.planes[0].bytes,
      format: im.Format.bgra,
    );
  }
}
