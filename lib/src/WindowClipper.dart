import 'package:flutter/material.dart';

class WindowClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.addRect(Rect.fromLTWH(0, 0, size.width, size.height));

    final x = (size.width * .1);
    final y = (size.height * .21);
    final width = (size.width * .8);
    final height = (width * .6296);

    path.addRRect(
      RRect.fromRectAndRadius(
        Rect.fromLTWH(x, y, width, height),
        const Radius.circular(15),
      ),
    );
    path.fillType = PathFillType.evenOdd;
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
