import 'dart:typed_data';

class Identification {
  //clave de elector
  String cve;
  Uint8List? image;
  Identification({required this.cve});
}
