import 'dart:async';

import 'package:camera/camera.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:google_ml_kit/google_ml_kit.dart';
import 'package:image/image.dart' as im;

import 'package:identification_vision/identification_vision.dart';
import 'package:identification_vision/src/ImageUtils.dart';
import 'package:identification_vision/src/WindowClipper.dart';

class IdentificationVision<Identification> extends StatefulWidget {
  final Widget loadingWidget;

  final Widget widgetCameraMask;

  final int? width;
  final int? height;
  final int quality;
  IdentificationVision({
    Key? key,
    this.width,
    this.height,
    this.loadingWidget = const Center(child: CircularProgressIndicator()),
    this.widgetCameraMask = const SizedBox(),
    this.quality = 100,
  }) : super(key: key) {
    assert((width != null && height != null) || width == height);
    assert(quality > -1 && quality < 101);
  }

  @override
  State<IdentificationVision> createState() => _IdentificationVisionState();
}

class _IdentificationVisionState extends State<IdentificationVision> {
  CameraController? _camera;
  bool _proscesando = false;

  bool get isInicialized => (_camera?.value.isInitialized ?? false);

  @override
  void initState() {
    super.initState();
    init();
  }

  @override
  void dispose() async {
    super.dispose();
    if (isInicialized && (_camera?.value.isStreamingImages ?? false)) {
      await _camera?.stopImageStream();
    }
    await _camera?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return !isInicialized
        ? widget.loadingWidget
        : SafeArea(
            child: AspectRatio(
              aspectRatio: 1 / .6296,
              child: Stack(
                children: [
                  CameraPreview(_camera!),
                  ClipPath(
                    clipper: WindowClipper(),
                    child: Container(
                      width: double.infinity,
                      height: double.infinity,
                      color: Colors.black38,
                    ),
                  ),
                  widget.widgetCameraMask,
                ],
              ),
            ),
          );
  }

  init() async {
    try {
      final cameras = await availableCameras();

      assert(cameras.isNotEmpty);

      _camera = CameraController(
        cameras[0],
        ResolutionPreset.high,
        enableAudio: false,
      );

      await _camera!.initialize();

      await _camera!.setFlashMode(FlashMode.auto);

      setState(() {});

      assert(isInicialized);

      _camera?.startImageStream(_onStream);
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
    }
  }

  Future<void> _onStream(CameraImage cameraImage) async {
    if (!mounted || _proscesando || !isInicialized) return;
    _proscesando = true;
    try {
      final Identification? id = await _proscesaImagen(cameraImage);
      if (id?.cve != null && mounted && Navigator.canPop(context)) {
        await _camera?.stopImageStream();
        Navigator.pop(context, id);
      } else {
        _proscesando = false;
      }
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
      _proscesando = false;
    }
  }

  Future<RecognisedText> getRecognisedTextFromInputImage(
      InputImage inputImage) async {
    TextDetector textDetector = GoogleMlKit.vision.textDetector();
    final dat = await textDetector.processImage(inputImage);
    await textDetector.close();
    return dat;
  }

  Future<Rect?> getRectFace(InputImage inputImage) async {
    FaceDetector faceDetector = GoogleMlKit.vision.faceDetector();
    final faces = await faceDetector.processImage(inputImage);
    await faceDetector.close();
    if (faces.isEmpty) {
      return null;
    }
    return faces.first.boundingBox;
  }

  Future<Identification?> getIdentificationFromRecognizedText(
      RecognisedText rt) async {
    final r = RegExp(r".*ELE[CG]T[O|0]R\s*([\w ]{18,})\n");
    final match = r.firstMatch(rt.text);
    final clave = match?.group(1);
    if (clave == null) return null;
    return Identification(cve: clave);
  }

  Future<Identification?> _proscesaImagen(CameraImage cameraImage) async {
    assert(mounted && isInicialized);

    try {
      final inputImage = ImageUtils.instance().getInputImagefromCameraImage(
          cameraImage, _camera!.description.sensorOrientation);

      final recognisedText = await getRecognisedTextFromInputImage(inputImage);

      if (recognisedText.blocks.isEmpty) {
        return null;
      }

      final ide = await getIdentificationFromRecognizedText(recognisedText);

      if (ide == null) {
        return null;
      }

      final faceRect = await getRectFace(inputImage);

      if (faceRect == null) {
        return null;
      }

      im.Image image =
          await ImageUtils.instance().cameraImageToImageLib(cameraImage);

      image = im.copyRotate(image, 90);

      int margin = (image.width * .07).toInt();
      final x = (image.width * .1).toInt();
      final y = (image.height * .21).toInt();
      final width = (image.width * .8).toInt();
      final height = (width * .6296).toInt();

      final captureRect = Rect.fromLTWH(
          x.toDouble(), y.toDouble(), width.toDouble(), height.toDouble());

      if (!captureRect.contains(recognisedText.blocks.first.rect.topLeft
        ..translate(-margin.toDouble(), -margin.toDouble()))) {
        return null;
      }
      if (!captureRect.contains(recognisedText.blocks.first.rect.bottomRight
        ..translate(margin.toDouble(), margin.toDouble()))) {
        return null;
      }

      image = im.copyCrop(image, x - margin, y - margin, width + margin * 2,
          height + margin * 2);
      if (widget.height != null && widget.height != null) {
        image =
            im.copyResize(image, height: widget.height, width: widget.width);
      }
      ide.image = ImageUtils.instance()
          .convertImageLibtoMemoryUint8List(image, quality: widget.quality);
      return ide;
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
    }
    return null;
  }
}
