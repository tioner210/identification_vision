import 'package:flutter/material.dart';
import 'package:identification_vision/identification_vision.dart';

Future<Identification?> launchIdentificationVision(BuildContext context,
    {int? width, int? height, int? quality}) async {
  return await Navigator.push<Identification?>(
    context,
    MaterialPageRoute<Identification?>(
      builder: (BuildContext context) => IdentificationVision<Identification?>(
        height: height,
        width: width,
        quality: quality ?? 100,
      ),
    ),
  );
}
